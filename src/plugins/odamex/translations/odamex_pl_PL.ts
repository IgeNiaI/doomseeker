<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>OdamexAboutProvider</name>
    <message>
        <location filename="../odamexengineplugin.cpp" line="49"/>
        <source>This plugin is distributed under the terms of the LGPL v2.1 or later.

</source>
        <translation>Ta wtyczka jest rozprowadzana na zasadach licencji LGPL v2.1 lub późniejszej.

</translation>
    </message>
</context>
<context>
    <name>OdamexGameInfo</name>
    <message>
        <source>Duel</source>
        <translation type="vanished">Pojedynek</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="28"/>
        <source>Items respawn</source>
        <translation>Respawn przedmiotów</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="29"/>
        <source>Weapons stay</source>
        <translation>Bronie zostają</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="30"/>
        <source>Friendly fire</source>
        <translation>Drużynowe obrażenia</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="31"/>
        <source>Allow exit</source>
        <translation>Pozwól na wyjście</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="32"/>
        <source>Infinite ammo</source>
        <translation>Nieskończona amunicja</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="33"/>
        <source>No monsters</source>
        <translation>Brak potworów</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="34"/>
        <source>Monsters respawn</source>
        <translation>Potwory odradzają się</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="35"/>
        <source>Fast monsters</source>
        <translation>Szybkie potwory</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="36"/>
        <source>Jumping allowed</source>
        <translation>Skakanie dozwolone</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="37"/>
        <source>Freelook allowed</source>
        <translation>Patrzenie góra-dół dozwolone</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="38"/>
        <source>Wad can be downloaded</source>
        <translation>Wad może zostać pobrany</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="39"/>
        <source>Server resets on empty</source>
        <translation>Serwer ulega resetowi gdy jest pusty</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="40"/>
        <source>Clean Maps</source>
        <translation>Wyczyść mapy</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="42"/>
        <source>Kill anyone who tries to leave the level</source>
        <translation>Zabij każdego, kto spróbuje opuścić poziom</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="52"/>
        <source>Lives</source>
        <translation>Życia</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="53"/>
        <source>Teams</source>
        <translation>Drużyny</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="54"/>
        <source>Attack &amp;&amp; Defend</source>
        <translation>Atak i Obrona</translation>
    </message>
</context>
<context>
    <name>OdamexGameMode</name>
    <message>
        <location filename="../odamexgamemode.cpp" line="34"/>
        <location filename="../odamexgamemode.cpp" line="54"/>
        <source>Horde</source>
        <translation>Horda</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="48"/>
        <source>Duel</source>
        <translation>Pojedynek</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="49"/>
        <source>Survival</source>
        <translation>Surwiwal</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="50"/>
        <source>Last Marine Standing</source>
        <translation>Last Marine Standing</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="51"/>
        <source>Teams Last Marine Standing</source>
        <translation>Drużynowy Last Marine Standing</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="52"/>
        <source>Attack &amp; Defend CTF</source>
        <translation>Atak i Obrona CTF</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="53"/>
        <source>LMS Capture The Flag</source>
        <translation>LMS Capture The Flag</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="55"/>
        <source>Survival Horde</source>
        <translation>Horda Surwiwal</translation>
    </message>
</context>
</TS>
