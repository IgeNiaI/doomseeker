<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>EngineZandronumConfigBox</name>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="45"/>
        <source>Testing releases</source>
        <translation>Versions de prova</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="48"/>
        <source>Directory for testing releases:</source>
        <translation>Directori per a les versions de prova:</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="55"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="66"/>
        <source>Doomseeker - choose Zandronum testing directory</source>
        <translation>Doomseeker - triï el directori de proves de Zandronum</translation>
    </message>
</context>
<context>
    <name>FlagsPage</name>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="78"/>
        <source>Zandronum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="106"/>
        <source>Zandronum 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="107"/>
        <source>Zandronum 2 (old)</source>
        <translation>Zandronum 2 (vell)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="109"/>
        <source>None</source>
        <translation>Res</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="110"/>
        <source>Old (ZDoom)</source>
        <translation>Vell (ZDoom)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="111"/>
        <source>Hexen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="112"/>
        <source>Strife</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="122"/>
        <source>Allowed</source>
        <translation>Permès</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="123"/>
        <source>Only team</source>
        <translation>Només equips</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="124"/>
        <source>Forbidden</source>
        <translation>Prohibit</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="126"/>
        <source>Automatic</source>
        <translation>Automàtic</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="127"/>
        <source>Unknown</source>
        <translation>Desconegut</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="214"/>
        <source>Default</source>
        <translation>Per defecte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="215"/>
        <source>No</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="216"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="359"/>
        <source>Unknown Zandronum version in the config. Reverting to default.</source>
        <translation>Versió desconeguda de Zandronum en la configuració. Es reverteix a predeterminat.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="386"/>
        <source>Tried to set unknown Zandronum version. Reverting to default.</source>
        <translation>S&apos;ha intentat establir una versió desconeguda de Zandronum. Revertint a la predeterminada.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="45"/>
        <source>Game version:</source>
        <translation>Versió del joc:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="68"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;If this is checked then Zandronum may override some of the settings selected here.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(+sv_defaultdmflags)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Si això està marcat, llavors Zandronum pot anul·lar algunes de les configuracions seleccionades aquí.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(+sv_defaultdmflags)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="76"/>
        <source>Default DMFlags</source>
        <translation>DMFlags Per omissió</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="90"/>
        <source>Environment</source>
        <translation>Entorn</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="101"/>
        <source>Falling damage:</source>
        <translation>Dany de caiguda:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="132"/>
        <source>No monsters</source>
        <translation>Sense monstres</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="155"/>
        <source>Items respawn</source>
        <translation>Els articles reapareixen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="162"/>
        <source>Barrels respawn</source>
        <translation>Els barrils reapareixen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="169"/>
        <source>Respawn invulnerability and invisibility spheres.</source>
        <translation>Fer reaparèixer les esferes de invulnerabilitat i invisibilitat.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="172"/>
        <source>Mega powerups respawn</source>
        <translation>Mega potenciadors reapareixen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="257"/>
        <source>Teams</source>
        <translation>Equips</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="263"/>
        <source>Server picks teams</source>
        <translation>El servidor tria equips</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="270"/>
        <source>Players can&apos;t switch teams</source>
        <translation>Els jugadors no poden canviar d&apos;equip</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="277"/>
        <source>Keep teams after a map change</source>
        <translation>Mantenir equips després d&apos;un canvi de mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="300"/>
        <source>Hide allies on the automap</source>
        <translation>Amaga aliats al Automap</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="307"/>
        <source>Don&apos;t let players spy on allies</source>
        <translation>No permetre que els jugadors espiïn als seus aliats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="330"/>
        <source>Instant flag/skull return</source>
        <translation>Retorn de bandera/calavera instantani</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="190"/>
        <source>Don&apos;t use ping-based backwards reconciliation for player-fired hitscans and rails.</source>
        <translation>No utilitzar reconciliació cap enrere basada en ping per hitscans i rails disparats per jugadors.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="193"/>
        <source>No unlagged</source>
        <translation>Sense unlagged</translation>
    </message>
    <message>
        <source>Apply lmsspectatorsettings in all game modes.</source>
        <translation type="vanished">Aplicar lmsspectatorsettings en tots els modes de joc.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1404"/>
        <source>Always apply LMS spectator settings</source>
        <translation>Sempre aplica la configuració d&apos;espectador LMS</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="344"/>
        <source>Enforces clients not to show medals, i.e. behave as if cl_medals == 0.</source>
        <translation>Obliga els clients a no mostrar medalles, és a dir, comportar-se com si cl_medals == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="347"/>
        <source>No medals</source>
        <translation>Sense medalles</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="24"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="378"/>
        <source>Disallow</source>
        <translation>No permetre</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="386"/>
        <source>Suicide</source>
        <translation>Suïcidi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="393"/>
        <source>Respawn</source>
        <translation>Reaparèixer</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="400"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;P_RadiusAttack doesn&apos;t give players any z-momentum if the attack was made by a player. This essentially disables rocket jumping.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;P_RadiusAttack no dóna als jugadors cap z-momentum si l&apos;atac s&apos;ha fet per un jugador. Això essencialment desactiva el salt de coet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="403"/>
        <source>Rocket jump</source>
        <translation>Salt de coet</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="410"/>
        <source>Taunt</source>
        <translation>Burlar-se</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="417"/>
        <source>Item drop</source>
        <translation>Deixar anar objecte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="444"/>
        <source>Use automap</source>
        <translation>Utilitzar Automap</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="451"/>
        <source>Turn off translucency</source>
        <translation>Desactiva la translucidesa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="458"/>
        <source>Use crosshairs</source>
        <translation>Utilitza punts de mira</translation>
    </message>
    <message>
        <source>Use custom GL lighting settings</source>
        <translation type="vanished">Utilitza configuracions d&apos;il·luminació GL personalitzades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="472"/>
        <source>Enforces clients not to identify players, i.e. behave as if cl_identifytarget == 0.</source>
        <translation>Obliga els clients a no identificar jugadors, és a dir, comportar-se com si cl_identifytarget == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="475"/>
        <source>Target identify</source>
        <translation>Identificar objectiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="482"/>
        <source>Enforces clients not to draw coop info, i.e. behave as if cl_drawcoopinfo == 0.</source>
        <translation>Obliga els clients a no dibuixar la informació del cooperatiu, és a dir, comportar-se com si cl_drawcoopinfo == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="485"/>
        <source>Display coop info</source>
        <translation>Mostra informació cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="510"/>
        <source>Use autoaim</source>
        <translation>Usa autoapuntat</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="517"/>
        <source>Only let the arbitrator set FOV (for all players)</source>
        <translation>Només deixeu que l&apos;àrbitre estableixi el FOV (per a tots els jugadors)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="520"/>
        <source>Use FOV</source>
        <translation>Utilitza FOV</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="527"/>
        <source>Use freelook</source>
        <translation>Utilitza càmera lliure</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players are not allowed to use the land CCMD. Because of Zandronum&apos;s default amount of air control, flying players can get a huge speed boast with the land CCMD. Disallowing players to land, allows to keep the default air control most people are used to while not giving flying players too much of an advantage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els jugadors no poden usar el CCMD terrestre. A causa de la quantitat predeterminada de control en aire de Zandronum, els jugadors que volen poden arribar a una gran velocitat amb el CCMD terrestre. Impedint que els jugadors aterrin, permet mantenir el control d&apos;aire predeterminat al qual la majoria de la gent està acostumada, mentre que no els dóna massa avantatge als jugadors que volen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="537"/>
        <source>Use &apos;land&apos; console command</source>
        <translation>Utilitza la comanda de consola &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="544"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Don&apos;t allow players to change how strongly will their screen flash when they get hit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;No permetre que els jugadors canviïn la intensitat del flaix quan reben un cop.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="547"/>
        <source>Change bloodied screen brightness</source>
        <translation>Canviar la brillantor de la pantalla ensagnada</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="569"/>
        <source>Abilities</source>
        <translation>Habilitats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="580"/>
        <source>Jumping:</source>
        <translation>Saltar:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="590"/>
        <source>Crouching:</source>
        <translation>Ajupir-se:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="622"/>
        <source>Infinite inventory</source>
        <translation>Inventari infinit</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="629"/>
        <source>Infinite ammo</source>
        <translation>Munició infinita</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="636"/>
        <source>Like Quake 3</source>
        <translation>Com Quake 3</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="639"/>
        <source>Slowly lose health when over 100%</source>
        <translation>Perdre lentament la salut per sobre del 100%</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="646"/>
        <source>Can use chasecam</source>
        <translation>Pot usar la càmera de persecució</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="653"/>
        <source>Allow BFG freeaiming</source>
        <translation>Permet apuntat lliure de la BFG</translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation type="vanished">Comportament</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="119"/>
        <source>Players can walk through each other</source>
        <translation>Els jugadors poden caminar entre ells</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="120"/>
        <source>Allies can walk through each other</source>
        <translation>Els aliats poden travessar-se entre si</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="118"/>
        <source>Players block each other normally</source>
        <translation>Els jugadors es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="660"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>No comprovar munició quan es canvia d&apos;armes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="702"/>
        <source>Force inactive players to spectate after:</source>
        <translation>Força als jugadors inactius al mode espectador després:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="725"/>
        <source>min.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="370"/>
        <source>Players</source>
        <translation>Jugadors</translation>
    </message>
    <message>
        <source>Score damage, not kills</source>
        <translation type="vanished">Puntua el mal, no els assassinats</translation>
    </message>
    <message>
        <source>Don&apos;t spawn Deathmatch weapons</source>
        <translation type="vanished">No fer aparèixer armes de Deathmatch</translation>
    </message>
    <message>
        <source>Spawn map actors in coop as if the game was single player.</source>
        <translation type="vanished">Fer aparèixer a actors del mapa en cooperatiu com si el joc fós d&apos;un sol jugador.</translation>
    </message>
    <message>
        <source>Don&apos;t spawn any multiplayer actor in coop</source>
        <translation type="vanished">No generis cap actor multijugador en el cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="791"/>
        <source>Monsters...</source>
        <translation>Monstres...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="797"/>
        <source>are fast (like Nightmare)</source>
        <translation>són ràpids (com en Nightmare)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="804"/>
        <source>respawn (like Nightmare)</source>
        <translation>reapareixen (com en Nightmare)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="811"/>
        <source>must be killed to enable exit</source>
        <translation>han de ser assassinats per poder sortir</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="820"/>
        <source>Kill percentage:</source>
        <translation>Percentatge d&apos;assassinats:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="840"/>
        <source>Multiplier of damage dealt by monsters.</source>
        <translation>Multiplicador del dany infligit pels monstres.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="843"/>
        <source>Damage factor:</source>
        <translation>Factor de Dany:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="850"/>
        <source>&lt;html&gt;&lt;body&gt;&lt;p&gt;Multiplier of damage dealt by monsters.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;&lt;p&gt;Multiplicador del dany infligit pels monstres.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="871"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Mata a tots els monstres generats per un cub de &quot;boss&quot; quan el &quot;boss&quot; mor</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="946"/>
        <source>On player death...</source>
        <translation>Quant el jugador mor...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="952"/>
        <source>respawn where died</source>
        <translation>reaparèixer on ha mort</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="959"/>
        <source>lose all inventory</source>
        <translation>perdre tot l&apos;inventari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="966"/>
        <source>lose armor</source>
        <translation>perdre armadura</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="973"/>
        <source>lose keys</source>
        <translation>perdre claus</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="980"/>
        <source>lose powerups</source>
        <translation>perdre potenciadors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="987"/>
        <source>lose weapons</source>
        <translation>perdre armes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="994"/>
        <source>lose all ammo</source>
        <translation>perdre tota la munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1001"/>
        <source>lose half ammo</source>
        <translation>perdre la meitat de la munició</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Affects game modes where &amp;quot;max. lives&amp;quot; can be used. If set, players who become dead spectators (run out of lives) will still keep inventory in accordance to the &amp;quot;Lose *&amp;quot; flags above. If unset, players who lose all lives will always lose entire inventory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afecta les maneres de joc on &amp;quot;max. vides&amp;quot; pot ser usat. Si s&apos;estableix, els jugadors que es converteixin en espectadors morts (es quedin sense vida) seguiran mantenint l&apos;inventari d&apos;acord amb &amp;quot;Perdre *&amp;quot; indicadors amunt. Si no s&apos;estableix, els jugadors que perdin totes les vides perdran sempre l&apos;inventari complet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="909"/>
        <source>Players who lose all lives can keep inventory</source>
        <translation>Els jugadors que perdin totes les vides poden conservar l&apos;inventari</translation>
    </message>
    <message>
        <source>Share keys between players</source>
        <translation type="vanished">Compartir claus entre jugadors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="894"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players will be respawned with full lives but the map will not be reset. Inventory will be preserved in accordance to &amp;quot;Lose inventory&amp;quot; flags. Players will be able to continue from the point where they died.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els jugadors seran reapareguts amb vides completes però el mapa no es reiniciarà. L&apos;inventari es conservarà d&apos;acord amb els indicadors de &amp;quot;Perdre inventari&amp;quot;. Els jugadors podran continuar des del punt on van morir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Survival only: no map reset when all players die</source>
        <translation type="vanished">Només supervivència: no es restableix el mapa quan tots els jugadors moren</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="752"/>
        <source>Cooperative</source>
        <translation>Cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1035"/>
        <source>When players die, they...</source>
        <translation>Quan els jugadors moren, ells...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1055"/>
        <source>respawn automatically</source>
        <translation>reaparèixer automàticament</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1048"/>
        <source>drop their weapon</source>
        <translation>deixar anar l&apos;arma</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1069"/>
        <source>respawn farthest away from others</source>
        <translation>reaparèixer el més lluny possible dels altres</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1076"/>
        <source>lose a frag</source>
        <translation>perdre un frag</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1041"/>
        <source>respawn with a shotgun</source>
        <translation>reaparèixer amb una escopeta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1062"/>
        <source>don&apos;t get respawn protection</source>
        <translation>no obtenir protecció de reaparició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1154"/>
        <source>When someone exits the level...</source>
        <translation>Quan algú surt del nivell ...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1160"/>
        <source>continue to the next map</source>
        <translation>continuar al següent mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1170"/>
        <source>restart the current level</source>
        <translation>reiniciar el nivell actual</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1177"/>
        <source>kill the player</source>
        <translation>mata al jugador</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1200"/>
        <source>Keep frags after map change</source>
        <translation>Mantenir frags després del canvi de mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1212"/>
        <source>Weapons &amp;&amp; ammo</source>
        <translation>Armes i munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1218"/>
        <source>Weapons stay after pickup</source>
        <translation>Les armes romanen després de recollir-les</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1225"/>
        <source>Double ammo</source>
        <translation>Doble munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="765"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1235"/>
        <source>Don&apos;t spawn...</source>
        <translation>No fer aparèixer...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1241"/>
        <source>health</source>
        <translation>salut</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1248"/>
        <source>armor</source>
        <translation>armadura</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1255"/>
        <source>runes</source>
        <translation>runes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1029"/>
        <source>Deathmatch</source>
        <translation>Combat a mort</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1291"/>
        <source>Weapons</source>
        <translation>Armes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1297"/>
        <source>Chainsaw</source>
        <translation>Serra mecànica</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1304"/>
        <source>Pistol</source>
        <translation>Pistola</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1311"/>
        <source>Shotgun</source>
        <translation>Escopeta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1318"/>
        <source>Super shotgun</source>
        <translation>Súper escopeta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1325"/>
        <source>Chaingun</source>
        <translation>Metralladora</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1332"/>
        <source>Minigun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1339"/>
        <source>Rocket launcher</source>
        <translation>Llançacoets</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1346"/>
        <source>Grenade launcher</source>
        <translation>Llançagranades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1353"/>
        <source>Plasma rifle</source>
        <translation>Rifle de plasma</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1360"/>
        <source>Railgun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1458"/>
        <source>lmsallowedweapons:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1425"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1448"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2092"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2102"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2112"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2143"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2160"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2170"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1435"/>
        <source>lmsspectatorsettings:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1372"/>
        <source>Spectators can...</source>
        <translation>Els espectadors poden...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1378"/>
        <source>talk to active players</source>
        <translation>parlar amb jugadors actius</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1385"/>
        <source>view the game</source>
        <translation>veure el joc</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1283"/>
        <source>LMS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1511"/>
        <source>Enable buggier wall clipping so players can wallrun.</source>
        <translation>Habilitar un atravessament de paret pitjor perquè els jugadors puguin fer wallrun.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1514"/>
        <source>Enable wall running</source>
        <translation>Activa wall running</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1521"/>
        <source>Pickups are only heard locally.</source>
        <translation>Les recollides d&apos;objectes només s&apos;escolten localment.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1524"/>
        <source>Don&apos;t let others hear pickups</source>
        <translation>No deixis que altres sentin la recollida d&apos;objectes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1531"/>
        <source>Allow instant respawn</source>
        <translation>Permetre reaparició instantània</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1538"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Disable stealth monsters, since doom2.exe didn&apos;t have them.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: this handles ZDoom&apos;s invisible monsters.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;THIS DOESN&apos;T AFFECT THE PARTIAL INVISIBILITY SPHERE IN ANY WAY. See &amp;quot;Monsters see semi-invisible players&amp;quot; for that.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deshabilita els monstres silenciosos, ja que doom2.exe no els tenia.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Nota: això fa anar els monstres invisibles de ZDoom.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;AIXÒ NO AFECTA L&apos;ESFERA DE INVISIBILITAT PARCIAL DE QUALSEVOL FORMA. Veure &amp;quot;Els monstres veuen jugadors semiinvisibles&amp;quot; per això.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1541"/>
        <source>Disable stealth monsters</source>
        <translation>Deshabilitar monstres silenciosos</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1548"/>
        <source>Limit actors to one sound at a time.</source>
        <translation>Limitar els actors a un so alhora.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1551"/>
        <source>Allow silent BFG trick</source>
        <translation>Permetre el truc de l&apos;BFG silenciós</translation>
    </message>
    <message>
        <source>Clients use the vanilla Doom weapon on pickup behavior.</source>
        <translation type="vanished">Els clients usen el comportament original de Doom en recollir d&apos;armes.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1561"/>
        <source>Original weapon switch</source>
        <translation>Canvi d&apos;armes original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1568"/>
        <source>Limited movement in the air</source>
        <translation>Moviment limitat en l&apos;aire</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1575"/>
        <source>This affects the partial invisibility sphere.</source>
        <translation>Això afecta l&apos;esfera d&apos;invisibilitat parcial.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1578"/>
        <source>Monsters see semi-invisible players</source>
        <translation>Els monstres veuen els jugadors semi-invisibles</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1585"/>
        <source>Allow the map01 &quot;plasma bump&quot; bug.</source>
        <translation>Permetre error &quot;plasma bump&quot; al map01.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1588"/>
        <source>Plasma bump bug</source>
        <translation>Error plasma bump</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1595"/>
        <source>Any boss death activates map specials</source>
        <translation>Qualsevol mort de &quot;Boss&quot; activa especials del mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1602"/>
        <source>Friction/pushers/pullers affect Monsters</source>
        <translation>Fricció/empentes/estrebades afecten els monstres</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1609"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Crushed monsters are turned into gibs, rather than replaced by gibs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els monstres aixafats esdevenen vísceres, en lloc de ser reemplaçats per aquestes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1612"/>
        <source>Crusher gibs by morphing, not replacement</source>
        <translation>Aixafar transforma en vísceres, en lloc de reemplaçar per aquestes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1619"/>
        <source>Block monster lines ignore friendly monsters</source>
        <translation>Les línies de bloqueig de monstres ignoren els monstres amistosos</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1626"/>
        <source>Find neighboring light level like Doom</source>
        <translation>Trobar el nivell de llum proper com en Doom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1637"/>
        <source>Use doom2.exe&apos;s original intermission screens/music.</source>
        <translation>Utilitza la pantalla/música originals de l&apos;intermedi de missió de doom2.exe.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1640"/>
        <source>Use old intermission screens/music</source>
        <translation>Utilitza pantalla/música d&apos;intermedi de missió antigues</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1647"/>
        <source>Scrolling sectors are additive like in Boom.</source>
        <translation>Els sectors de desplaçament són additius com en Boom.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1650"/>
        <source>Scrolling sectors are additive</source>
        <translation>Els sectors de desplaçament són additius</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1657"/>
        <source>Sector sounds use original method for sound origin.</source>
        <translation>Els sons del sector usen el mètode original per a l&apos;origen del so.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1660"/>
        <source>Sector sounds use original method</source>
        <translation>Els sons del sector fan servir el mètode original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1667"/>
        <source>Monsters cannot move when hanging over a dropoff.</source>
        <translation>Els monstres no es poden moure quan estan sobre un abisme.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1670"/>
        <source>No monsters dropoff move</source>
        <translation>Sense monstres moure en abisme</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1684"/>
        <source>Instantly moving floors are not silent.</source>
        <translation>Els pisos que es mouen a l&apos;instant no són silenciosos.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1687"/>
        <source>Instantly moving floors aren&apos;t silent</source>
        <translation>Els pisos que es mouen a l&apos;instant no són silenciosos</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1694"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clients send ucmd.buttons as &amp;quot;long&amp;quot; instead of as &amp;quot;byte&amp;quot; in CLIENTCOMMANDS_ClientMove. So far this is only necessary if the ACS function GetPlayerInput is used in a server side script to check for buttons bigger than BT_ZOOM. Otherwise this information is completely useless for the server and the additional net traffic to send it should be avoided.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els clients envien ucmd.buttons com &amp;quot;long&amp;quot; en lloc de com &amp;quot;byte&amp;quot; en CLIENTCOMMANDS_ClientMove. Fins ara, això només és necessari si la funció ACS GetPlayerInput s&apos;utilitza en un script del costat del servidor per buscar botons més grans que BT_ZOOM. Altrament, aquesta informació és completament inútil per al servidor i s&apos;ha d&apos;evitar el tràfic de xarxa addicional per a enviar-lo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1697"/>
        <source>Clients send full button info</source>
        <translation>Els clients envien tota la informació sobre els botons</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1704"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use Doom&apos;s random table instead of ZDoom&apos;s random number generator. Affects weapon damage among other things.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Utilitza la taula aleatòria de Doom en lloc del generador de nombres aleatoris de ZDoom. Afecta el dany de les armes entre altres coses.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1707"/>
        <source>Old random number generator</source>
        <translation>Antic generador de nombres aleatoris</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1677"/>
        <source>Monsters can&apos;t be pushed off cliffs</source>
        <translation>Els monstres no poden ser empesos a abismes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1717"/>
        <source>Old damage radius (infinite height)</source>
        <translation>Ràdio de dany antic (alçada infinita)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1724"/>
        <source>Minotaur&apos;s floor flame explodes immediately when feet are clipped</source>
        <translation>El foc del sòl del minotaure explota immediatament quan els peus el travessen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1731"/>
        <source>Original velocity calc. for A_Mushroom in Dehacked</source>
        <translation>Càlcul de velocitat original per A_Mushroom a Dehacked</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1738"/>
        <source>Sprite sort order inverted for sprites of equal distance</source>
        <translation>Ordre de Sprites invertit per sprites en la mateixa distància</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1745"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Els Hitscans usen el mapa de blocs original i el codi de verificació d&apos;impacte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1752"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Dibuixar poliobjectes com als bons (i antics) temps</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1501"/>
        <source>Compatibility</source>
        <translation>Compatibilitat</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="182"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="200"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Don&apos;t use backwards reconcilation for the hitscan tracers fired from the BFG9000.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;No usis &quot;reconciliació inversa&quot; per als traçadors llençats per la BFG9000.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="203"/>
        <source>No unlagged on BFG tracers</source>
        <translation>Sense unlagged per als traçadors de BFG</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="227"/>
        <source>Server country:</source>
        <translation>País del servidor:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="234"/>
        <source>&lt;p&gt;Sets the country of the server, which will be presented to server browsers.&lt;/p&gt; &lt;p&gt;The value of this variable can be one of:&lt;ul&gt;&lt;li&gt;an ISO 3166-1 alpha-3 country code, for example GBR for the United Kingdom, USA for the United States&lt;/li&gt;&lt;li&gt;&amp;quot;automatic&amp;quot;, to tell the launcher to use IP geolocation (default)&lt;/li&gt;&lt;li&gt;or &amp;quot;unknown&amp;quot;.&lt;/li&gt;&lt;/p&gt;

&lt;p&gt;This was introduced to allow hosts to combat the inaccuracies of the IP geolocation that launchers rely on. Not all servers browsers may support this feature and will use the IP geolocation instead.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Defineix el país del servidor, que es mostrarà als sercadors de servidors.&lt;/p&gt; &lt;p&gt;El valor d&apos;aquesta variable pot ser un dels següents:&lt;ul&gt;&lt;li&gt;un codi de país en format ISO 3166-1 alpha-3. Per exemple GBR per al Regne Unit, o USA per als Estats Units d&apos;Amèrica.&lt;/li&gt;&lt;li&gt;&amp;quot;automatic&amp;quot;, Per a indicar al llançador que faci servir geolocalització IP (per defecte)&lt;/li&gt;&lt;li&gt;o &amp;quot;unknown&amp;quot;.&lt;/li&gt;&lt;/p&gt;

&lt;p&gt;Aquesta opció es va introduir per a combatre resultats erronis que la geolocalització IP donava als llançadors. No tots els cercadors de servidors suporten aquesta opció, i usaran la geolocalització IP en el seu lloc&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="465"/>
        <source>Use custom renderer settings</source>
        <translation>Usar opcions de renderitzat personalitzades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="554"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If set, stops doors from being manually closed. This prevents players intentionally (or unintentionally) griefing by closing doors that other players (or the same player) have opened.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si s&apos;activa, prevé que les portes es tanquin automàticament. Això evita que els jugadors intencionadament (o no) causin baixades de rendiment al servidor tancant portes que un altre jugador (o ell mateix) havia obert.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="557"/>
        <source>Closing doors</source>
        <translation>Tancament de portes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="603"/>
        <source>Blocking:</source>
        <translation>Bloquejant:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="610"/>
        <source>Private chat:</source>
        <translation>Chat privat:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="667"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Forces the player&apos;s pitch to be limited to what&apos;s allowed in the software renderer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Força el rang de visió vertical entre els límits del renderitzador de software.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="670"/>
        <source>Limit vertical look range to the limit of the software renderer</source>
        <translation>Limita el rang de visió vertical al del renderitzador vertical</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="677"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Allows players to fire hitscans and projectiles through teammates.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Permet als jugadors llançar traçadors i projectils a través dels seus companys d&apos;equip.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="680"/>
        <source>Players can shoot through allies</source>
        <translation>Els jugadors poden disparar a través d&apos;aliats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="687"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players aren&apos;t pushed by attacks caused by their teammates (e.g. BFG tracers).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els jugadors no són empentats per atacs causats per aliats (per exemple, BFG).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="690"/>
        <source>Players don&apos;t push allies when shooting them</source>
        <translation>No empentar aliats en disparar-los</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="771"/>
        <source>Deathmatch weapons</source>
        <translation>Armes de combat a mort</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="778"/>
        <source>&lt;p&gt;Spawn map items, monsters and other objects in coop as if the game was single player.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Genera elements de mapes, monstres i altres objectes en mode cooperatiu com si el joc fos per a un sol jugador.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="781"/>
        <source>any multiplayer actor in coop</source>
        <translation>Qualsevol actor de multijugador en cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="878"/>
        <source>Don&apos;t count monsters in the &quot;end level&quot; sector towards kills</source>
        <translation>No comptis com a baixes els monstres al sector &quot;end level&quot;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="888"/>
        <source>Survival</source>
        <translation>Supervivència</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="897"/>
        <source>No map reset when all players die</source>
        <translation>No es restableix el mapa quan tots els jugadors moren</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="904"/>
        <source>&lt;p&gt;If set, players who lose all lives and become dead spectators will still keep inventory in accordance to the &amp;quot;Lose inventory&amp;quot; flags. When they will finally be able to play again, all their items will be returned to them.&lt;/p&gt;

&lt;p&gt;If unset, players who lose all lives will always lose their entire inventory, regardless of the &amp;quot;Lose inventory&amp;quot; flags.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si s&apos;activa, els jugadors que es quedin sense vides mantindran el seu inventari d&apos;acord amb les flags de &amp;quot;Perdre inventari&amp;quot;. Quan ressuscitin, recuperaran els seus objectes.&lt;/p&gt;

&lt;p&gt;Si no, no se&apos;ls retornarà res, independentment de &amp;quot;Perdre inventari&amp;quot;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="923"/>
        <source>Players...</source>
        <translation>Jugadors...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="929"/>
        <source>score damage, not kills</source>
        <translation>Puntua el mal, no els assassinats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="936"/>
        <source>share keys</source>
        <translation>Compartir claus</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1098"/>
        <source>sec.</source>
        <translation>seg.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1114"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;How long a player must wait after dying before they can respawn. This doesn&apos;t apply to players who are spawn telefragged.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Quant de temps ha d&apos;esperar un jugador abans de reaparèixer. No s&apos;aplica a jugadors que són telegrafiats.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1139"/>
        <source>can respawn only after:</source>
        <translation>Pot reaparèixer després de:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1395"/>
        <source>LMS flags</source>
        <translation>flags LMS</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1401"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Apply lmsspectatorsettings in all game modes, not only in LMS.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aplicar lmsspectatorsettings a tots els jocs, no només LMS.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1558"/>
        <source>When a weapon is picked up, the weapon switch behaves like in vanilla Doom.</source>
        <translation>Quan una arma s&apos;agafa, es fa el canvi d&apos;armes del Doom vanilla.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1714"/>
        <source>&lt;p&gt;Shooting a rocket against a wall will damage the actors near this wall even if they are far below or above the explosion.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Disparar un coet a una paret farà mal a tots els actors que estan a prop, independentment de l&apos;altura.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1787"/>
        <source>&lt;p&gt;This allows for decorations to be pass-through for projectiles as they were originally in Doom.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Això permet que les decoracions es puguin travessar pels projectils, com al Doom original.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1790"/>
        <source>Use original missile clipping height</source>
        <translation>Utilitzar l&apos;alçada original d&apos;atravessament dels míssils</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1797"/>
        <source>Use sector based sound target code</source>
        <translation>Utilitza codi de so objectiu basat en sectors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1804"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Doom&apos;s hitscan tracing code ignores all lines with both sides in the same sector. ZDoom&apos;s does not. This option reverts to the original but less precise behavior. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;El codi d&apos;escaneig d&apos;impactes de Doom ignora totes les línies amb banda i banda en el mateix sector. ZDoom no. Aquesta opció reverteix al comportament original però és menys precís. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1807"/>
        <source>Trace ignore lines w/ same sector on both sides</source>
        <translation>El traç ignora les línies amb el mateix sector a banda i banda</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1814"/>
        <source>Limit deh.MaxHealth to health bonus</source>
        <translation>Limitar deh.MaxHealth a la bonificació de salut</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1821"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The scrolling floor specials in Heretic and Hexen move the player much faster than the actual texture scrolling speed. Enable this option to restore this effect. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els desplaçaments especials del sòl en Heretic i Hexen mouen al jugador molt més ràpid que la velocitat de desplaçament de la textura real. Habilita aquesta opció per restaurar aquest efecte.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1824"/>
        <source>Raven&apos;s scrollers use original speed</source>
        <translation>Els scrollers de Raven fan servir la seva velocitat original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1831"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add NOGRAVITY to actors named InvulnerabilitySphere, Soulsphere, Megasphere and BlurSphere when spawned by the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afegiu NOGRAVITY als actors anomenats InvulnerabilitySphere, Soulsphere, Megasphere i BlurSphere quan apareixen pel mapa.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1834"/>
        <source>Add NOGRAVITY flag to spheres</source>
        <translation>Afegir l&apos;indicador NOGRAVITY a les esferes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1841"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When a player leaves the game, don&apos;t stop any scripts of that player that are still running.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Quan un jugador abandona el joc, no aturar cap script que aquest jugador encara estigui executant.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1844"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>No aturar els scripts dels jugadors al desconnectar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1851"/>
        <source>&lt;p&gt;If this is enabled, explosions cause a strong horizontal thrust like in old ZDoom versions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si això està habilitat, les explosions causen una forta empenta horitzontal com en versions antigues de ZDoom.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1854"/>
        <source>Old ZDoom horizontal thrust</source>
        <translation>Empenta horitzontal de l&apos;antic ZDoom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1861"/>
        <source>&lt;p&gt;If this is enabled, non-SOLID things like flags fall through bridges (as they used to do in old ZDoom versions).&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si això està habilitat, elements no SÒLIDS com banderes cauen a través de ponts (com solien fer en versions antigues de ZDoom).&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1864"/>
        <source>Old ZDoom bridge drops</source>
        <translation>Caigudes de pont de l&apos;antic ZDoom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1871"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uses old ZDoom jump physics, it&apos;s a minor bug in the gravity code that causes gravity application in the wrong place.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fes servir l&apos;antiga física de salt ZDoom, és un error menor en el codi de gravetat que causa l&apos;aplicació de la gravetat en el lloc equivocat.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1874"/>
        <source>ZDoom 1.23B33 jump physics</source>
        <translation>Físiques de salt de ZDoom 1.23B33</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1881"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Zandronum uses more tracers to fill in the gaps, this reverts it to vanilla&apos;s 3 tracer behavior&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Zandronum fa servir més traçadors per omplir els buits, això ho reverteix al comportament original de 3 traçadors&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1884"/>
        <source>Use vanilla autoaim tracer behavior</source>
        <translation>Utilitza el comportament del traçador d&apos;autoapuntat original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1891"/>
        <source>Ignore compositing when drawing masked midtextures</source>
        <translation>Ignorar la composició mentre es dibuixa textures mitjanes emmascarades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1898"/>
        <source>It is impossible to face directly NSEW</source>
        <translation>És impossible mirar directament a l&apos;adreça cardinal</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1905"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;World and global-scope ACS variables/arrays will be reset upon resetting the map like in survival.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;les variables ACS World i scope-global es reiniciaràn amb el mapa, com en mode supervivència.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1908"/>
        <source>Reset global/world ACS vars on map reset</source>
        <translation>Reinicia les variables ACS global/world amb el reinici de mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1919"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use Doom&apos;s shortest texture find behavior. This is requied by some WADs in MAP07.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Utilitza el comportament de recerca de textura més curta de Doom. Això és requerit per alguns WADs a MAP07.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1922"/>
        <source>Find shortest textures like Doom</source>
        <translation>Troba les textures més curtes com en Doom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1929"/>
        <source>Limit pain elementals to 20 lost souls</source>
        <translation>Limitar els elementals de dolor a 20 ànimes perdudes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1936"/>
        <source>Spawned item drops on the floor</source>
        <translation>L&apos;element generat cau a terra</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1943"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Treat ACS scripts with the SCRIPTF_Net flag to be client side, i.e. executed on the clients, but not on the server.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tractar els scripts ACS amb l&apos;indicador SCRIPTF_Net perquè s&apos;executin en el costat del client, és a dir, executats en els clients, però no al servidor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1946"/>
        <source>NET scripts are clientside</source>
        <translation>Els scripts NET són del costat del client</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1953"/>
        <source>&lt;p&gt;Like in vanilla Doom.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Com el Doom vanilla.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1956"/>
        <source>Actors are infinitely tall</source>
        <translation>Els actors són infinitament alts</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1963"/>
        <source>Don&apos;t fix loop index for stair building.</source>
        <translation>No arreglar l&apos;índex del bucle per construir escales.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1966"/>
        <source>Use buggier stair building</source>
        <translation>Utilitza un constructor d&apos;escales amb més errors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1973"/>
        <source>Disable Boom door light effect</source>
        <translation>Deshabilitar l&apos;efecte de llum de porta BOOM</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1980"/>
        <source>All special lines can drop use lines</source>
        <translation>Totes les línies especials poden deixar anar línies d&apos;ús</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1987"/>
        <source>Original sound curve</source>
        <translation>Corba de so original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1994"/>
        <source>Disallow weapon change until fully drawn or hidden</source>
        <translation>No permetre el canvi d&apos;arma fins que estigui completament dibuixat o ocult</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2001"/>
        <source>West spawns are silent</source>
        <translation>Les aparicions &quot;West&quot; són silencioses</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2008"/>
        <source>Use the same floor motion behavior as Doom</source>
        <translation>Utilitza el comportament de moviment del pis com a Doom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1777"/>
        <source>Compatibility 2</source>
        <translation>Compatibilitat 2</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2015"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the jumping behavior known from Skulltag.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usar el comportament de salt de Skulltag.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2018"/>
        <source>Skulltag jumping</source>
        <translation>Salt Skulltag</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2025"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Prevent obituary messages from being printed to the console when a player dies.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Evitar que els missatges d&apos;obituari s&apos;imprimeixin a la consola quan un jugador mor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2028"/>
        <source>Don&apos;t display player obituaries</source>
        <translation>No mostrar obituaris</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2035"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Disable the window check in CheckForPushSpecial(). For maps that expect non-blocking push lines to activate when you are standing on them and run into a completely different line. Some maps may need this to enable climbing up ladders.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Desactiveu la comprovació de la finestra a CheckForPushSpecial(). Per als mapes que esperen que s&apos;activin línies d&apos;empenta sense bloqueig quan us trobeu sobre d&apos;elles i correu a una línia completament diferent. És possible que alguns mapes ho necessitin per poder pujar per escales.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2038"/>
        <source>Non-blocking lines can be pushed</source>
        <translation>Les línies sense bloqueig es poden empènyer</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2063"/>
        <source>Voting</source>
        <translation>Votacions</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2085"/>
        <source>zandronum dmflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2078"/>
        <source>dmflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2136"/>
        <source>dmflags2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2129"/>
        <source>zandronum compatflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2122"/>
        <source>compatflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2153"/>
        <source>compatflags2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Time limit:</source>
        <translation type="vanished">Límit de temps:</translation>
    </message>
    <message>
        <source>Frag limit:</source>
        <translation type="vanished">Límit de frags:</translation>
    </message>
    <message>
        <source>Point limit:</source>
        <translation type="vanished">Límit de punts:</translation>
    </message>
    <message>
        <source>Win limit:</source>
        <translation type="vanished">Límit de victòries:</translation>
    </message>
    <message>
        <source>Duel limit:</source>
        <translation type="vanished">Límit de duels:</translation>
    </message>
    <message>
        <source>Max. lives:</source>
        <translation type="vanished">Max. vides:</translation>
    </message>
    <message>
        <location filename="../zandronumserver.h" line="72"/>
        <source>&lt;&lt; Unknown &gt;&gt;</source>
        <translation>&lt;&lt; Desconegut &gt;&gt;</translation>
    </message>
</context>
<context>
    <name>TestingProgressDialog</name>
    <message>
        <location filename="../zandronumbinaries.cpp" line="453"/>
        <source>Downloading testing binaries...</source>
        <translation>Descarregant arxius binaris de prova ...</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="453"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="463"/>
        <source>Doomseeker</source>
        <translation>Doomseeker</translation>
    </message>
</context>
<context>
    <name>VotingSetupWidget</name>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="32"/>
        <source>Use this page</source>
        <translation>Utilitzeu aquesta pàgina</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="54"/>
        <source>Who can vote</source>
        <translation>Qui pot votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="60"/>
        <source>All can vote</source>
        <translation>Tots poden votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="70"/>
        <source>No one can vote</source>
        <translation>Ningú pot votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="77"/>
        <source>Spectators can&apos;t vote</source>
        <translation>Els espectadors no poden votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="102"/>
        <source>Minimum number of players required to call a vote:</source>
        <translation>Nombre mínim de jugadors necessaris per a sol·licitar una votació:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="134"/>
        <source>Vote cooldown:</source>
        <translation>Temps entre vots:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="155"/>
        <source>&lt;p&gt;Cooldown for the same type of vote is the double of this value. Setting this to 0 disables it.&lt;/p&gt;</source>
        <translation>&lt;p&gt;El temps és el doble per a vots del mateix tipus. Posar-ho a 0 ho desactiva.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="171"/>
        <source>min.</source>
        <translation>min.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="180"/>
        <source>Time before client can vote after connecting:</source>
        <translation>Temps abans no un nou client pugui votar:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="214"/>
        <source>sec.</source>
        <translation>seg.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="225"/>
        <source>Allow specific votes</source>
        <translation>Permetre vots específics</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="231"/>
        <source>duellimit</source>
        <translation>límit de duel</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="241"/>
        <source>pointlimit</source>
        <translation>límit de punts</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="251"/>
        <source>timelimit</source>
        <translation>límit de temps</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="261"/>
        <source>winlimit</source>
        <translation>límit de victòries</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="271"/>
        <source>changemap</source>
        <translation>Canviar mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="281"/>
        <source>kick</source>
        <translation>expulsar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="291"/>
        <source>map</source>
        <translation>mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="301"/>
        <source>fraglimit</source>
        <translation>límit de frags</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="311"/>
        <source>force players to spectate</source>
        <translation>obligar els jugadors al mode espectador</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="321"/>
        <source>flags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="328"/>
        <source>next map</source>
        <translation>Mapa següent</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="338"/>
        <source>next secret</source>
        <translation>Secret següent</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="351"/>
        <source>Vote flooding protection enabled</source>
        <translation>Protecció contra inundacions de vots habilitada</translation>
    </message>
</context>
<context>
    <name>Zandronum2::Dmflags</name>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="40"/>
        <source>Use Doom&apos;s shortest texture behavior</source>
        <translation>Utilitza el comportament de textura més ràpid de Doom</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="42"/>
        <source>Don&apos;t fix loop index for stair building</source>
        <translation>No arreglar l&apos;índex del bucle per a la construcció d&apos;escales</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="44"/>
        <source>Pain elemental is limited to 20 lost souls</source>
        <translation>Pain elemental està limitat a 20 ànimes perdudes</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="46"/>
        <source>Pickups are only heard locally</source>
        <translation>Recollir articles només es poden escoltar localment</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="48"/>
        <source>Infinitely tall actors</source>
        <translation>Actors infinitament alts</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="50"/>
        <source>Limit actors to only one sound</source>
        <translation>Limitar els actors a un sol so</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="52"/>
        <source>Enable wallrunning</source>
        <translation>Habilitar wallrunning</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="54"/>
        <source>Dropped items spawn on floor</source>
        <translation>Els objectes deixats anar apareixen a terra</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="56"/>
        <source>Special lines block use line</source>
        <translation>Línies especials bloquegen l&apos;ús de línies</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="58"/>
        <source>Disable BOOM local door light effect</source>
        <translation>Deshabilitar l&apos;efecte de llum de porta local BOOM</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="60"/>
        <source>Raven&apos;s scrollers use their original speed</source>
        <translation>Els scrollers de Raven fan servir la seva velocitat original</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="62"/>
        <source>Use sector based sound target code</source>
        <translation>Utilitza codi de so objectiu basat en sectors</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="64"/>
        <source>Limit dehacked MaxHealth to health bonus</source>
        <translation>Limitar dehacked MaxHealth a la bonificació de vida</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="66"/>
        <source>Trace ignores lines with the same sector on both sides</source>
        <translation>El tracer ignora les línies amb el mateix sector a banda i banda</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="68"/>
        <source>Monsters can not move when hanging over a drop off</source>
        <translation>Els monstres no es poden moure quan estan sobre un abisme</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="70"/>
        <source>Scrolling sectors are additive like Boom</source>
        <translation>Els sectors de desplaçament són additius com Boom</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="72"/>
        <source>Monsters can see semi-invisible players</source>
        <translation>Els monstres poden veure jugadors semi-invisibles</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="74"/>
        <source>Instantly moving floors are not silent</source>
        <translation>Els pisos que es mouen a l&apos;instant no dir res</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="76"/>
        <source>Sector sounds use original method for sound origin</source>
        <translation>Els sons de sector usen el mètode original per a l&apos;origen del so</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="78"/>
        <source>Use original Doom heights for clipping against projectiles</source>
        <translation>Utilitzar altures originals de Doom durant una col·lisió amb míssils</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="80"/>
        <source>Monsters can&apos;t be pushed over dropoffs</source>
        <translation>Els monstres no poden ser empesos a abismes</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="82"/>
        <source>Any monster which calls BOSSDEATH counts for level specials</source>
        <translation>Qualsevol monstre que truqui a BOSSDEATH compten per a nivells especials</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="84"/>
        <source>Minotaur&apos;s floor flame is exploded immediately when feet are clipped</source>
        <translation>El foc del sòl del minotaure explota immediatament quan els peus el travessen</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="86"/>
        <source>Force original velocity calculations for A_Mushroom in Dehacked mods</source>
        <translation>Forçar càlculs de velocitat originals per A_Mushroom a mods Dehacked</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="88"/>
        <source>Monsters are affected by friction and pushers/pullers</source>
        <translation>Els monstres es veuen afectats per la fricció i empentes/estrebades</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="90"/>
        <source>Crushed monsters are turned into gibs, rather than replaced by gibs</source>
        <translation>Els monstres aixafats esdevenen vísceres, en lloc de ser reemplaçats per aquestes</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="92"/>
        <source>Friendly monsters aren&apos;t blocked by monster-blocking lines</source>
        <translation>Els monstres amistosos ignoren les línies de bloqueig de monstres</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="94"/>
        <source>Invert sprite sorting order for sprites of equal distance</source>
        <translation>Invertir ordre de classificació de sprites per sprites d&apos;igual distància</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="96"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Els Hitscans usen el mapa de blocs original i el codi de verificació d&apos;impacte</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="98"/>
        <source>Find neighboring light level like like Doom</source>
        <translation>Busqui el nivell de llum veïna com en Doom</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="100"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Dibuixar poliobjectes com als bons (i antics) temps</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="110"/>
        <source>Net scripts are client side</source>
        <translation>Els scripts NET s&apos;executen en el client</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="112"/>
        <source>Clients send full button info</source>
        <translation>Els clients envien tota la informació sobre els botons</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="114"/>
        <source>Players can&apos;t use &apos;land&apos; CCMD</source>
        <translation>Els jugadors no poden usar el comandament &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="117"/>
        <source>Use Doom&apos;s original random number generator</source>
        <translation>Utilitzar el generador de xifres aleatòries original de Doom</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="119"/>
        <source>Spheres have NOGRAVITY flag</source>
        <translation>Les esferes tenen l&apos;indicador NOGRAVITY</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="122"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>No aturar els scripts dels jugadors al desconnectar</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="125"/>
        <source>Use horizontal explosion thrust of old ZDoom versions</source>
        <translation>Utilitzar l&apos;empenta d&apos;explosió horitzontal de versions antigues de ZDoom</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="128"/>
        <source>Non-SOLID things fall through invisible bridges</source>
        <translation>Coses no sòlides cauen a través de ponts invisibles</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="130"/>
        <source>Use old ZDoom jump physics</source>
        <translation>Fes servir físiques velles de salt de ZDoom</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="133"/>
        <source>Disallow weapon change when in mid raise/lower</source>
        <translation>No permetre el canvi d&apos;arma quan està a mig pujar/baixar</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="135"/>
        <source>Use vanilla&apos;s autoaim tracer behavior</source>
        <translation>Utilitzar el comportament de autoapuntat original</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="137"/>
        <source>West spawns are silent</source>
        <translation>Les aparicions &quot;West&quot; són silencioses</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="139"/>
        <source>Limited movement in the air</source>
        <translation>Moviment limitat en l&apos;aire</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="141"/>
        <source>Allow map01 &quot;plasma bump&quot; bug</source>
        <translation>Permetre error &quot;plasma bump&quot; al map01</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="143"/>
        <source>Allow instant respawn after death</source>
        <translation>Permetre reaparició instantània després de morir</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="145"/>
        <source>Disable taunting</source>
        <translation>Desactivar burles</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="147"/>
        <source>Use doom2.exe&apos;s original sound curve</source>
        <translation>Utilitza la corba sonora original de doom2.exe</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="149"/>
        <source>Use original doom2 intermission music</source>
        <translation>Utilitzar la música original d&apos;intermedi de doom2</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="151"/>
        <source>Disable stealth monsters</source>
        <translation>Deshabilitar monstres silenciosos</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="153"/>
        <source>Radius damage has infinite height</source>
        <translation>El radi de dany té una alçada infinita</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="155"/>
        <source>Disable crosshair</source>
        <translation>Desactivar espiell</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="157"/>
        <source>Force weapon switch</source>
        <translation>Forçar el canvi d&apos;arma</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="165"/>
        <source>Do not spawn health items (DM)</source>
        <translation>No fer aparèixer ítems de salut (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="167"/>
        <source>Do not spawn powerups (DM)</source>
        <translation>No fer aparèixer potenciadors (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="169"/>
        <source>Weapons remain after pickup (DM)</source>
        <translation>Les armes romanen després de recollir-les (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="171"/>
        <source>Falling damage (old ZDoom)</source>
        <translation>Dany de caiguda (antic ZDoom)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="173"/>
        <source>Falling damage (Hexen)</source>
        <translation>Dany de caiguda (Hexen)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="175"/>
        <source>Falling damage (Strife)</source>
        <translation>Dany de caiguda (Strife)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="177"/>
        <source>Stay on same map when someone exits (DM)</source>
        <translation>Romandre al mapa quan algú ho acabi (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="179"/>
        <source>Spawn players as far as possible (DM)</source>
        <translation>Fer aparèixer els jugadors el més lluny possible (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="181"/>
        <source>Automatically respawn dead players (DM)</source>
        <translation>Reaparició automàtica de jugadors morts (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="183"/>
        <source>Don&apos;t spawn armor (DM)</source>
        <translation>No fer aparèixer armadura (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="185"/>
        <source>Kill anyone who tries to exit the level (DM)</source>
        <translation>Mata a qualsevol que intenti acabar el nivell (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="187"/>
        <source>Infinite ammo</source>
        <translation>Munició infinita</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="189"/>
        <source>No monsters</source>
        <translation>Sense monstres</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="191"/>
        <source>Monsters respawn</source>
        <translation>Els monstres reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="193"/>
        <source>Items other than invuln. and invis. respawn</source>
        <translation>Els objectes (a excepció d&apos;invulnerabilitat i invisibilitat) reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="195"/>
        <source>Fast monsters</source>
        <translation>Monstres ràpids</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="197"/>
        <source>No jumping</source>
        <translation>No salts</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="199"/>
        <source>No freelook</source>
        <translation>No càmera lliure</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="201"/>
        <source>Respawn invulnerability and invisibility</source>
        <translation>Reaparició d&apos;invulnerabilitat i invisibilitat</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="203"/>
        <source>Arbitrator FOV</source>
        <translation>FOV preestablert</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="205"/>
        <source>No multiplayer weapons in cooperative</source>
        <translation>Sense armes multijugador en cooperatiu</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="207"/>
        <source>No crouching</source>
        <translation>Sense ajupir-se</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="209"/>
        <source>Lose all old inventory on respawn (COOP)</source>
        <translation>Perdre tot l&apos;inventari al reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="211"/>
        <source>Lose keys on respawn (COOP)</source>
        <translation>Perdre claus en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="213"/>
        <source>Lose weapons on respawn (COOP)</source>
        <translation>Perdre armes en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="215"/>
        <source>Lose armor on respawn (COOP)</source>
        <translation>Perdre armadura a reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="217"/>
        <source>Lose powerups on respawn (COOP)</source>
        <translation>Perdre potenciadors en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="219"/>
        <source>Lose ammo on respawn (COOP)</source>
        <translation>Perdre munició en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="221"/>
        <source>Lose half your ammo on respawn (COOP)</source>
        <translation>Perdre la meitat de la munició al reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="224"/>
        <source>Jumping allowed</source>
        <translation>Saltar permès</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="226"/>
        <source>Crouching allowed</source>
        <translation>Ajupir-se permès</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="234"/>
        <source>Drop weapons upon death</source>
        <translation>Deixar anar armes en morir</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="236"/>
        <source>Don&apos;t spawn runes</source>
        <translation>No generar runes</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="238"/>
        <source>Instantly return flags (ST/CTF)</source>
        <translation>Retornar banderes automàticament (ST/CTF)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="240"/>
        <source>Don&apos;t allow players to switch teams</source>
        <translation>No permetre que els jugadors canviïn d&apos;equip</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="242"/>
        <source>Players are automatically assigned teams</source>
        <translation>Als jugadors se&apos;ls assignen equips automàticament</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="244"/>
        <source>Double the amount of ammo given</source>
        <translation>Duplicar la quantitat de munició</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="246"/>
        <source>Players slowly lose health over 100% like Quake</source>
        <translation>Els jugadors perden lentament la salut per sobre del 100% com en Quake</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="248"/>
        <source>Allow BFG freeaiming</source>
        <translation>Permet apuntat lliure de la BFG</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="250"/>
        <source>Barrels respawn</source>
        <translation>Els barrils reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="252"/>
        <source>No respawn protection</source>
        <translation>No hi ha protecció de reaparició</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="254"/>
        <source>All players start with a shotgun</source>
        <translation>Tots els jugadors comencen amb una escopeta</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="256"/>
        <source>Players respawn where they died (COOP)</source>
        <translation>Els jugadors reapareixen on van morir (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="258"/>
        <source>Don&apos;t clear frags after each level</source>
        <translation>No esborrar indicadors després de cada nivell</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="260"/>
        <source>Player can&apos;t respawn</source>
        <translation>El jugador no pot reaparèixer</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="262"/>
        <source>Lose a frag when killed</source>
        <translation>Perdre un frag en morir</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="264"/>
        <source>Infinite inventory</source>
        <translation>Inventari infinit</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="266"/>
        <source>All monsters must be killed before exiting</source>
        <translation>Tots els monstres han de ser assassinats abans de sortir</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="268"/>
        <source>Players can&apos;t see the automap</source>
        <translation>Els jugadors no poden veure el Automap</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="270"/>
        <source>Allies can&apos;t be seen on the automap</source>
        <translation>Els aliats no es poden veure al Automap</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="272"/>
        <source>You can&apos;t spy allies</source>
        <translation>No pots espiar als teus aliats</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="274"/>
        <source>Players can use chase cam</source>
        <translation>Els jugadors poden fer servir la càmera de persecució</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="276"/>
        <source>Players can&apos;t suicide</source>
        <translation>Els jugadors no poden suïcidar-se</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="278"/>
        <source>Players can&apos;t use autoaim</source>
        <translation>Els jugadors no poden usar autoapuntat</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="280"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>No comprovar munició quan es canvia d&apos;armes</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="282"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Mata a tots els monstres generats per un cub de &quot;boss&quot; quan el &quot;boss&quot; mor</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="290"/>
        <source>Clients can&apos;t identify targets</source>
        <translation>Els clients no poden identificar els objectius</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="293"/>
        <source>lmsspectatorsettings applied in all game modes</source>
        <translation>Apliqui lmsspectatorsettings en tots els modes de joc</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="295"/>
        <source>Clients can&apos;t draw coop info</source>
        <translation>Els clients no poden usar el CVar &quot;Coop Info&quot;</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="297"/>
        <source>Unlagged is disabled</source>
        <translation>Unlagged està deshabilitat</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="299"/>
        <source>Players don&apos;t block each other</source>
        <translation>Els jugadors no es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="301"/>
        <source>Clients don&apos;t show medals</source>
        <translation>Els clients no mostren medalles</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="303"/>
        <source>Keys are shared between players</source>
        <translation>Les claus es comparteixen entre jugadors</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="305"/>
        <source>Player teams are preserved between maps</source>
        <translation>Els equips es conserven entre mapes</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="307"/>
        <source>Force OpenGL defaults</source>
        <translation>Força els valors predeterminats d&apos;OpenGL</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="309"/>
        <source>No rocket jumping</source>
        <translation>Sense &quot;saltar coets&quot;</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="311"/>
        <source>Award damage instead of kills</source>
        <translation>Premiar dany en lloc d&apos;assassinats</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="313"/>
        <source>Force drawing alpha</source>
        <translation>Força dibuixant alfa</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="315"/>
        <source>Don&apos;t spawn multiplayer things</source>
        <translation>No fer aparèixer coses del multijugador</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="318"/>
        <source>Force blood screen brightness on clients to emulate vanilla</source>
        <translation>Força la brillantor de la sang en pantalla en els clients per emular l&apos;original</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="320"/>
        <source>Teammates don&apos;t block each other</source>
        <translation>Els companys d&apos;equip no es bloquegen entre ells</translation>
    </message>
</context>
<context>
    <name>Zandronum3::Dmflags</name>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="40"/>
        <source>Use Doom&apos;s shortest texture behavior</source>
        <translation>Utilitza el comportament de textura més ràpid de Doom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="42"/>
        <source>Don&apos;t fix loop index for stair building</source>
        <translation>No arreglar l&apos;índex del bucle per a la construcció d&apos;escales</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="44"/>
        <source>Pain elemental is limited to 20 lost souls</source>
        <translation>Pain elemental està limitat a 20 ànimes perdudes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="46"/>
        <source>Pickups are only heard locally</source>
        <translation>Recollir articles només es poden escoltar localment</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="48"/>
        <source>Infinitely tall actors</source>
        <translation>Actors infinitament alts</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="50"/>
        <source>Limit actors to only one sound</source>
        <translation>Limitar els actors a un sol so</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="52"/>
        <source>Enable wallrunning</source>
        <translation>Habilitar wallrunning</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="54"/>
        <source>Dropped items spawn on floor</source>
        <translation>Els objectes deixats anar apareixen a terra</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="56"/>
        <source>Special lines block use line</source>
        <translation>Línies especials bloquegen l&apos;ús de línies</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="58"/>
        <source>Disable BOOM local door light effect</source>
        <translation>Deshabilitar l&apos;efecte de llum de porta local BOOM</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="60"/>
        <source>Raven&apos;s scrollers use their original speed</source>
        <translation>Els scrollers de Raven fan servir la seva velocitat original</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="62"/>
        <source>Use sector based sound target code</source>
        <translation>Utilitza codi de so objectiu basat en sectors</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="64"/>
        <source>Limit dehacked MaxHealth to health bonus</source>
        <translation>Limitar dehacked MaxHealth a la bonificació de vida</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="66"/>
        <source>Trace ignores lines with the same sector on both sides</source>
        <translation>El tracer ignora les línies amb el mateix sector a banda i banda</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="68"/>
        <source>Monsters can not move when hanging over a drop off</source>
        <translation>Els monstres no es poden moure quan estan sobre un abisme</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="70"/>
        <source>Scrolling sectors are additive like Boom</source>
        <translation>Els sectors de desplaçament són additius com Boom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="72"/>
        <source>Monsters can see semi-invisible players</source>
        <translation>Els monstres poden veure jugadors semi-invisibles</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="74"/>
        <source>Instantly moving floors are not silent</source>
        <translation>Els pisos que es mouen a l&apos;instant no dir res</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="76"/>
        <source>Sector sounds use original method for sound origin</source>
        <translation>Els sons de sector usen el mètode original per a l&apos;origen del so</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="78"/>
        <source>Use original Doom heights for clipping against projectiles</source>
        <translation>Utilitzar altures originals de Doom durant una col·lisió amb míssils</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="80"/>
        <source>Monsters can&apos;t be pushed over dropoffs</source>
        <translation>Els monstres no poden ser empesos a abismes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="82"/>
        <source>Any monster which calls BOSSDEATH counts for level specials</source>
        <translation>Qualsevol monstre que truqui a BOSSDEATH compten per a nivells especials</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="84"/>
        <source>Minotaur&apos;s floor flame is exploded immediately when feet are clipped</source>
        <translation>El foc del sòl del minotaure explota immediatament quan els peus el travessen</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="86"/>
        <source>Force original velocity calculations for A_Mushroom in Dehacked mods</source>
        <translation>Forçar càlculs de velocitat originals per A_Mushroom a mods Dehacked</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="88"/>
        <source>Monsters are affected by friction and pushers/pullers</source>
        <translation>Els monstres es veuen afectats per la fricció i empentes/estrebades</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="90"/>
        <source>Crushed monsters are turned into gibs, rather than replaced by gibs</source>
        <translation>Els monstres aixafats esdevenen vísceres, en lloc de ser reemplaçats per aquestes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="92"/>
        <source>Friendly monsters aren&apos;t blocked by monster-blocking lines</source>
        <translation>Els monstres amistosos ignoren les línies de bloqueig de monstres</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="94"/>
        <source>Invert sprite sorting order for sprites of equal distance</source>
        <translation>Invertir ordre de classificació de sprites per sprites d&apos;igual distància</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="96"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Els Hitscans usen el mapa de blocs original i el codi de verificació d&apos;impacte</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="98"/>
        <source>Find neighboring light level like like Doom</source>
        <translation>Busqui el nivell de llum veïna com en Doom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="100"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Dibuixar poliobjectes com als bons (i antics) temps</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="102"/>
        <source>Ignore compositing when drawing masked midtextures</source>
        <translation>Ignorar la composició mentre es dibuixa textures mitjanes emmascarades</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="112"/>
        <source>It is impossible to directly face cardinal direction</source>
        <translation>És impossible mirar directament la direcció cardinal</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="114"/>
        <source>Use the same floor motion behavior as Doom</source>
        <translation>Utilitza el comportament de moviment del pis com a Doom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="116"/>
        <source>Trigger bump actions on pass-over linedefs</source>
        <translation>Activar accions de cop a linedefs de transferència</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="126"/>
        <source>Net scripts are client side</source>
        <translation>Els scripts NET s&apos;executen en el client</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="128"/>
        <source>Clients send full button info</source>
        <translation>Els clients envien tota la informació sobre els botons</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="130"/>
        <source>Players can&apos;t use &apos;land&apos; CCMD</source>
        <translation>Els jugadors no poden usar el comandament &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="133"/>
        <source>Use Doom&apos;s original random number generator</source>
        <translation>Utilitzar el generador de xifres aleatòries original de Doom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="135"/>
        <source>Spheres have NOGRAVITY flag</source>
        <translation>Les esferes tenen l&apos;indicador NOGRAVITY</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="138"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>No aturar els scripts dels jugadors al desconnectar</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="141"/>
        <source>Use horizontal explosion thrust of old ZDoom versions</source>
        <translation>Utilitzar l&apos;empenta d&apos;explosió horitzontal de versions antigues de ZDoom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="144"/>
        <source>Non-SOLID things fall through invisible bridges</source>
        <translation>Coses no sòlides cauen a través de ponts invisibles</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="146"/>
        <source>Use old ZDoom jump physics</source>
        <translation>Fes servir físiques velles de salt de ZDoom</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="149"/>
        <source>Disallow weapon change when in mid raise/lower</source>
        <translation>No permetre el canvi d&apos;arma quan està a mig pujar/baixar</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="151"/>
        <source>Use vanilla&apos;s autoaim tracer behavior</source>
        <translation>Utilitzar el comportament de autoapuntat original</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="153"/>
        <source>West spawns are silent</source>
        <translation>Les aparicions &quot;West&quot; són silencioses</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="155"/>
        <source>Jumping works as in Skulltag</source>
        <translation>Saltar funciona com a Skulltag</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="157"/>
        <source>Reset the world/global ACS variables when resetting the map</source>
        <translation>Reinicia les variables ACS global/world amb el reinici de mapa</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="159"/>
        <source>Don&apos;t show obituaries</source>
        <translation>No mostrar obituaris</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="161"/>
        <source>Limited movement in the air</source>
        <translation>Moviment limitat en l&apos;aire</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="163"/>
        <source>Allow map01 &quot;plasma bump&quot; bug</source>
        <translation>Permetre error &quot;plasma bump&quot; al map01</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="165"/>
        <source>Allow instant respawn after death</source>
        <translation>Permetre reaparició instantània després de morir</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="167"/>
        <source>Disable taunting</source>
        <translation>Desactivar burles</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="169"/>
        <source>Use doom2.exe&apos;s original sound curve</source>
        <translation>Utilitza la corba sonora original de doom2.exe</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="171"/>
        <source>Use original doom2 intermission music</source>
        <translation>Utilitzar la música original d&apos;intermedi de doom2</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="173"/>
        <source>Disable stealth monsters</source>
        <translation>Deshabilitar monstres silenciosos</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="175"/>
        <source>Radius damage has infinite height</source>
        <translation>El radi de dany té una alçada infinita</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="177"/>
        <source>Disable crosshair</source>
        <translation>Desactivar espiell</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="179"/>
        <source>Force weapon switch</source>
        <translation>Forçar el canvi d&apos;arma</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="187"/>
        <source>Do not spawn health items (DM)</source>
        <translation>No fer aparèixer ítems de salut (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="189"/>
        <source>Do not spawn powerups (DM)</source>
        <translation>No fer aparèixer potenciadors (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="191"/>
        <source>Weapons remain after pickup (DM)</source>
        <translation>Les armes romanen després de recollir-les (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="193"/>
        <source>Falling damage (old ZDoom)</source>
        <translation>Dany de caiguda (antic ZDoom)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="195"/>
        <source>Falling damage (Hexen)</source>
        <translation>Dany de caiguda (Hexen)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="197"/>
        <source>Falling damage (Strife)</source>
        <translation>Dany de caiguda (Strife)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="199"/>
        <source>Stay on same map when someone exits (DM)</source>
        <translation>Romandre al mapa quan algú ho acabi (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="201"/>
        <source>Spawn players as far as possible (DM)</source>
        <translation>Fer aparèixer els jugadors el més lluny possible (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="203"/>
        <source>Automatically respawn dead players (DM)</source>
        <translation>Reaparició automàtica de jugadors morts (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="205"/>
        <source>Don&apos;t spawn armor (DM)</source>
        <translation>No fer aparèixer armadura (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="207"/>
        <source>Kill anyone who tries to exit the level (DM)</source>
        <translation>Mata a qualsevol que intenti acabar el nivell (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="209"/>
        <source>Infinite ammo</source>
        <translation>Munició infinita</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="211"/>
        <source>No monsters</source>
        <translation>Sense monstres</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="213"/>
        <source>Monsters respawn</source>
        <translation>Els monstres reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="215"/>
        <source>Items other than invuln. and invis. respawn</source>
        <translation>Els objectes (a excepció d&apos;invulnerabilitat i invisibilitat) reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="217"/>
        <source>Fast monsters</source>
        <translation>Monstres ràpids</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="219"/>
        <source>No jumping</source>
        <translation>No salts</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="221"/>
        <source>No freelook</source>
        <translation>No càmera lliure</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="223"/>
        <source>Respawn invulnerability and invisibility</source>
        <translation>Reaparició d&apos;invulnerabilitat i invisibilitat</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="225"/>
        <source>Arbitrator FOV</source>
        <translation>FOV preestablert</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="227"/>
        <source>No multiplayer weapons in cooperative</source>
        <translation>Sense armes multijugador en cooperatiu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="229"/>
        <source>No crouching</source>
        <translation>Sense ajupir-se</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="231"/>
        <source>Lose all old inventory on respawn (COOP)</source>
        <translation>Perdre tot l&apos;inventari al reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="233"/>
        <source>Lose keys on respawn (COOP)</source>
        <translation>Perdre claus en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="235"/>
        <source>Lose weapons on respawn (COOP)</source>
        <translation>Perdre armes en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="237"/>
        <source>Lose armor on respawn (COOP)</source>
        <translation>Perdre armadura a reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="239"/>
        <source>Lose powerups on respawn (COOP)</source>
        <translation>Perdre potenciadors en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="241"/>
        <source>Lose ammo on respawn (COOP)</source>
        <translation>Perdre munició en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="243"/>
        <source>Lose half your ammo on respawn (COOP)</source>
        <translation>Perdre la meitat de la munició al reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="245"/>
        <source>Jumping allowed</source>
        <translation>Saltar permès</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="247"/>
        <source>Crouching allowed</source>
        <translation>Ajupir-se permès</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="255"/>
        <source>Drop weapons upon death</source>
        <translation>Deixar anar armes en morir</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="257"/>
        <source>Don&apos;t spawn runes</source>
        <translation>No generar runes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="259"/>
        <source>Instantly return flags (ST/CTF)</source>
        <translation>Retornar banderes automàticament (ST/CTF)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="261"/>
        <source>Don&apos;t allow players to switch teams</source>
        <translation>No permetre que els jugadors canviïn d&apos;equip</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="263"/>
        <source>Players are automatically assigned teams</source>
        <translation>Als jugadors se&apos;ls assignen equips automàticament</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="265"/>
        <source>Double the amount of ammo given</source>
        <translation>Duplicar la quantitat de munició</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="267"/>
        <source>Players slowly lose health over 100% like Quake</source>
        <translation>Els jugadors perden lentament la salut per sobre del 100% com en Quake</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="269"/>
        <source>Allow BFG freeaiming</source>
        <translation>Permet apuntat lliure de la BFG</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="271"/>
        <source>Barrels respawn</source>
        <translation>Els barrils reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="273"/>
        <source>No respawn protection</source>
        <translation>No hi ha protecció de reaparició</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="275"/>
        <source>All players start with a shotgun</source>
        <translation>Tots els jugadors comencen amb una escopeta</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="277"/>
        <source>Players respawn where they died (COOP)</source>
        <translation>Els jugadors reapareixen on van morir (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="279"/>
        <source>Don&apos;t clear frags after each level</source>
        <translation>No esborrar indicadors després de cada nivell</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="281"/>
        <source>Player can&apos;t respawn</source>
        <translation>El jugador no pot reaparèixer</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="283"/>
        <source>Lose a frag when killed</source>
        <translation>Perdre un frag en morir</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="285"/>
        <source>Infinite inventory</source>
        <translation>Inventari infinit</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="287"/>
        <source>All monsters must be killed before exiting</source>
        <translation>Tots els monstres han de ser assassinats abans de sortir</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="289"/>
        <source>Players can&apos;t see the automap</source>
        <translation>Els jugadors no poden veure el Automap</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="291"/>
        <source>Allies can&apos;t be seen on the automap</source>
        <translation>Els aliats no es poden veure al Automap</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="293"/>
        <source>You can&apos;t spy allies</source>
        <translation>No pots espiar als teus aliats</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="295"/>
        <source>Players can use chase cam</source>
        <translation>Els jugadors poden fer servir la càmera de persecució</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="297"/>
        <source>Players can&apos;t suicide</source>
        <translation>Els jugadors no poden suïcidar-se</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="299"/>
        <source>Players can&apos;t use autoaim</source>
        <translation>Els jugadors no poden usar autoapuntat</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="301"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>No comprovar munició quan es canvia d&apos;armes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="303"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Mata a tots els monstres generats per un cub de &quot;boss&quot; quan el &quot;boss&quot; mor</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="306"/>
        <source>Do not count monsters in &apos;end level when dying&apos; sectors towards kill count</source>
        <translation>No compti els assassinats de monstres en els sectors de &apos;fi de nivell en cas de mort&apos;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="316"/>
        <source>Clients can&apos;t identify targets</source>
        <translation>Els clients no poden identificar els objectius</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="319"/>
        <source>lmsspectatorsettings applied in all game modes</source>
        <translation>Apliqui lmsspectatorsettings en tots els modes de joc</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="321"/>
        <source>Clients can&apos;t draw coop info</source>
        <translation>Els clients no poden usar el CVar &quot;Coop Info&quot;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="323"/>
        <source>Unlagged is disabled</source>
        <translation>Unlagged està deshabilitat</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="325"/>
        <source>Players don&apos;t block each other</source>
        <translation>Els jugadors no es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="327"/>
        <source>Clients don&apos;t show medals</source>
        <translation>Els clients no mostren medalles</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="329"/>
        <source>Keys are shared between players</source>
        <translation>Les claus es comparteixen entre jugadors</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="331"/>
        <source>Player teams are preserved between maps</source>
        <translation>Els equips es conserven entre mapes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="333"/>
        <source>Force renderer defaults</source>
        <translation>Força els valors predeterminats de renderitzat</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="354"/>
        <source>No unlagged on BFG tracers</source>
        <translation>Sense unlagged per als traçadors de BFG</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="356"/>
        <source>No door closing</source>
        <translation>Sense tancament de portes</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="358"/>
        <source>Limit vertical look range to the limit of the software renderer</source>
        <translation>Limita el rang de visió vertical al del renderitzador vertical</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="360"/>
        <source>Players can shoot through allies</source>
        <translation>Els jugadors poden disparar a través d&apos;aliats</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="362"/>
        <source>Players don&apos;t push allies when shooting them</source>
        <translation>No empentar aliats en disparar-los</translation>
    </message>
    <message>
        <source>Force OpenGL defaults</source>
        <translation type="vanished">Força els valors predeterminats d&apos;OpenGL</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="335"/>
        <source>No rocket jumping</source>
        <translation>Sense &quot;saltar coets&quot;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="337"/>
        <source>Award damage instead of kills</source>
        <translation>Premiar dany en lloc d&apos;assassinats</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="339"/>
        <source>Force drawing alpha</source>
        <translation>Força dibuixant alfa</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="341"/>
        <source>Don&apos;t spawn multiplayer things</source>
        <translation>No fer aparèixer coses del multijugador</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="344"/>
        <source>Force blood screen brightness on clients to emulate vanilla</source>
        <translation>Força la brillantor de la sang en pantalla en els clients per emular l&apos;original</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="346"/>
        <source>Teammates don&apos;t block each other</source>
        <translation>Els companys d&apos;equip no es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="348"/>
        <source>No dropping allowed</source>
        <translation>No es permet deixar anar articles</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="350"/>
        <source>No map reset on death in survival</source>
        <translation>No es restableix el mapa al morir en supervivència</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="352"/>
        <source>Dead players can keep inventory</source>
        <translation>Els jugadors morts poden conservar l&apos;inventari</translation>
    </message>
</context>
<context>
    <name>ZandronumAboutProvider</name>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="110"/>
        <source>This plugin is distributed under the terms of the LGPL v2.1 or later.

</source>
        <translation>Aquest complement es distribueix sota els termes de la llicència LGPL v2.1 o posterior.

</translation>
    </message>
</context>
<context>
    <name>ZandronumBroadcast</name>
    <message>
        <location filename="../zandronumbroadcast.cpp" line="115"/>
        <source>Listening to Zandronum&apos;s LAN servers broadcasts on port %1.</source>
        <translation>Cercant transmissions de servidors Zandronum en LAN al port %1.</translation>
    </message>
    <message>
        <location filename="../zandronumbroadcast.cpp" line="121"/>
        <source>Failed to bind Zandronum&apos;s LAN broadcasts listening socket on port %1. Will keep retrying silently.</source>
        <translation>Error en enllaçar la connexió d&apos;escolta de les transmissions LAN de Zandronum al port %1. Seguirem reintentant-ho en silenci.</translation>
    </message>
</context>
<context>
    <name>ZandronumClientExeFile</name>
    <message>
        <location filename="../zandronumbinaries.cpp" line="94"/>
        <source>client</source>
        <translation>client</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="116"/>
        <source>No testing directory specified for Zandronum</source>
        <translation>No s&apos;ha especificat cap directori de proves per Zandronum</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="122"/>
        <source>Unable to create directory:
%1</source>
        <translation>No es pot crear directori:
%1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="126"/>
        <source>Unable to create directory:
%1/%2</source>
        <translation>No es pot crear directori:
%1/%2</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="149"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="150"/>
        <source>Please install now version &quot;%1&quot; into:
%2</source>
        <translation>Instal·li ara la versió &quot;%1&quot; a:
%2</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="165"/>
        <source>&lt;p&gt;Installation of testing binaries for version %1 can potentially &lt;b&gt;overwrite&lt;/b&gt; your files.&lt;/p&gt;&lt;p&gt;Game will be installed to:&lt;br&gt;%2&lt;/p&gt;&lt;p&gt;Do you want Doomseeker to extract Zandronum files, potentially &lt;b&gt;overwriting existing ones&lt;/b&gt;, and to copy all your configuration files from your base directory?&lt;/p&gt;</source>
        <translation>&lt;p&gt;La instal·lació dels binaris de prova per a la versió%1 pot potencialment &lt;b&gt;reescriure&lt;/b&gt; els teus arxius.&lt;/p&gt;&lt;p&gt;El joc s&apos;instal·larà a:&lt;br&gt;%2&lt;/p&gt;&lt;p&gt;Vols que Doomseeker extregui els arxius de Zandronum, potencialment &lt;b&gt;reescrivint els arxius ja existents&lt;/b&gt;, i copiar tots els arxius de configuració del teu directori base?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="176"/>
        <source>Zandronum testing version %1 can be installed.

Game will be installed to:
%2

Do you want Doomseeker to install that version and copy all your configuration files from your base directory?</source>
        <translation>La versió de prova de Zandronum%1 pot instal·lar-se.

El joc pot instal·lar-se a:
%2

Vols que Doomseeker instal aquesta versió i copieu tots els teus arxius de configuració del teu directori base?</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="183"/>
        <source>Doomseeker - install Zandronum testing version</source>
        <translation>Doomseeker - instal·lar la versió de prova de Zandronum</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="209"/>
        <source>No testing directory configured for Zandronum</source>
        <translation>No s&apos;ha configurat cap directori de proves per Zandronum</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="219"/>
        <source>%1
doesn&apos;t exist.
You need to install new testing binaries.</source>
        <translation>%1
No existeix.
Ha d&apos;instal·lar els nous executables de prova.</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="226"/>
        <source>%1
exists but is NOT a directory.
Cannot proceed.</source>
        <translation>%1
existeix però NO és un directori.
No es pot procedir.</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="235"/>
        <source>%1
exists but doesn&apos;t contain a Zandronum executable.

Doomseeker can still install the game there if you want.</source>
        <translation>%1
existeix, però no conté l&apos;executable de Zandronum.

Doomseeker pot instal lar el joc allà igualment si ho desitja.</translation>
    </message>
    <message>
        <source>%1
exists but doesn&apos;t contain Zandronum executable.

Doomseeker can still install the game there if you want.</source>
        <translation type="vanished">%1
existeix, però no conté l&apos;executable de Zandronum.

Doomseeker pot instal lar el joc allà igualment si ho desitja.</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="306"/>
        <source>Downloading Zandronum testing binary from URL: %1</source>
        <translation>Descarregant l&apos;arxiu binari de prova de Zandronum des de la URL: %1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="311"/>
        <source>Doomseeker - download failed</source>
        <translation>Doomseeker - descàrrega fallida</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="312"/>
        <source>Failed to download testing binary.

%1</source>
        <translation>Error al descarregar el binari de la versió de prova.

%1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="336"/>
        <source>Unpacking file: %1</source>
        <translation>Desempaquetant arxiu: %1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="348"/>
        <source>Doomseeker - unpack failed</source>
        <translation>Doomseeker - error en desempaquetar</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="349"/>
        <source>Failed to unpack: %1</source>
        <translation>Error al desempaquetar: %1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="376"/>
        <source>%1
 should be a script file but is a directory!</source>
        <translation>%1
 hauria de ser un script, però és un directori!</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="385"/>
        <source>You don&apos;t have permissions to execute file: %1
</source>
        <translation>No tens permisos per executar l&apos;arxiu: %1
</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="432"/>
        <source>Couldn&apos;t open batch file &quot;%1&quot; for writing</source>
        <translation>No s&apos;ha pogut obrir l&apos;script &quot;%1&quot; per escriure</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="439"/>
        <source>Error while writing batch file &quot;%1&quot;</source>
        <translation>Error en escriure l&apos;script &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="449"/>
        <source>Cannot set permissions for file:
%1</source>
        <translation>No es poden definir els permisos per a l&apos;arxiu:
%1</translation>
    </message>
</context>
<context>
    <name>ZandronumGameInfo</name>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="37"/>
        <source>Time limit</source>
        <translation>Límit de temps</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="45"/>
        <source>Frag limit</source>
        <translation>Límit de frags</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="56"/>
        <source>Point limit</source>
        <translation>Límit de punts</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="63"/>
        <source>Win limit</source>
        <translation>Límit de victòries</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="68"/>
        <source>Duel limit</source>
        <translation>Límit de duels</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="71"/>
        <source>Max. lives</source>
        <translation>Max. vides</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="80"/>
        <source>Survival</source>
        <translation>Supervivència</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="81"/>
        <source>Invasion</source>
        <translation>Invasió</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="84"/>
        <source>Duel</source>
        <translation>Duel</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="85"/>
        <source>Terminator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="86"/>
        <source>LMS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="87"/>
        <source>Team LMS</source>
        <translation>LMS per equips</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="88"/>
        <source>Possession</source>
        <translation>Possessió</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="89"/>
        <source>Team Poss</source>
        <translation>Possessió per equips</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="90"/>
        <source>Team Game</source>
        <translation>Joc per equips</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="92"/>
        <source>One Flag CTF</source>
        <translation>CTF amb una bandera</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="93"/>
        <source>Skulltag</source>
        <translation>Skulltag</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="94"/>
        <source>Domination</source>
        <translation>Dominació</translation>
    </message>
</context>
<context>
    <name>ZandronumMasterClient</name>
    <message>
        <source>You may contact Zandronum staff about this through IRC: &lt;b&gt;irc.zandronum.com #zandronum&lt;/b&gt; or on the forum: &lt;a href=&quot;https://zandronum.com/forum&quot;&gt;https://zandronum.com/forum.&lt;/a&gt;</source>
        <translation type="vanished">Pot posar-se en contacte amb el personal de Zandronum a través d&apos;IRC: &lt;b&gt;irc.zandronum.com #zandronum&lt;/b&gt; o al fòrum: &lt;a href=&quot;https://zandronum.com/forum&quot;&gt;https://zandronum.com/forum.&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../zandronummasterclient.cpp" line="70"/>
        <source>You can contact Zandronum staff about this on Discord or on the forum: &lt;a href=&quot;https://zandronum.com/forum&quot;&gt;https://zandronum.com/forum&lt;/a&gt;.</source>
        <translation>Pot posar-se en contacte amb el personal de Zandronum a través de Discord o al fòrum: &lt;a href=&quot;https://zandronum.com/forum&quot;&gt;https://zandronum.com/forum&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>ZandronumRConProtocol</name>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="69"/>
        <source>Connection attempt ...</source>
        <translation>Intent de connexió ...</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="77"/>
        <source>Too many failed connection attempts. Aborting.</source>
        <translation>Massa intents fallits de connexió. Avortant.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="112"/>
        <source>Authenticating ...</source>
        <translation>Autenticant ...</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="131"/>
        <source>Too many failed authentication attempts. Aborting.</source>
        <translation>Massa intents d&apos;autenticació fallits. Avortant.</translation>
    </message>
    <message numerus="yes">
        <location filename="../zandronumrconprotocol.cpp" line="173"/>
        <source>Delaying for about %n seconds before next authentication attempt.</source>
        <translation>
            <numerusform>Retard d&apos;aproximadament %n segon abans del pròxim intent d&apos;autenticació.</numerusform>
            <numerusform>Retard d&apos;aproximadament %n segons abans del pròxim intent d&apos;autenticació.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="220"/>
        <source>Failed to establish connection.</source>
        <translation>Error en establir la connexió.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="223"/>
        <source>Timeout on authentication.</source>
        <translation>Límit de temps excedit en l&apos;autenticació.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="241"/>
        <source>You have been banned from this server.</source>
        <translation>Has estat expulsat d&apos;aquest servidor.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="246"/>
        <source>The protocol appears to be outdated.</source>
        <translation>El protocol sembla estar desactualitzat.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="284"/>
        <source>Authentication failure.</source>
        <translation>Error d’autenticació.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="289"/>
        <source>Remote console connection established.</source>
        <translation>Connexió a la consola remota establerta.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="290"/>
        <source>-----</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ZandronumServer</name>
    <message>
        <location filename="../zandronumserver.cpp" line="112"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../zandronumserver.cpp" line="113"/>
        <source>Red</source>
        <translation>Vermell</translation>
    </message>
    <message>
        <location filename="../zandronumserver.cpp" line="114"/>
        <source>Green</source>
        <translation>Verd</translation>
    </message>
    <message>
        <location filename="../zandronumserver.cpp" line="115"/>
        <source>Gold</source>
        <translation>Or</translation>
    </message>
</context>
</TS>
