//------------------------------------------------------------------------------
// ip2ccountry.h
//------------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301  USA
//
//------------------------------------------------------------------------------
// Copyright (C) 2021 "Zalewa" <zalewapl@gmail.com>
//------------------------------------------------------------------------------
#ifndef DOOMSEEKER_IP2C_IP2CCOUNTRY_H
#define DOOMSEEKER_IP2C_IP2CCOUNTRY_H

#include "global.h"

#include <QPixmap>
#include <QString>

/**
 * @brief Flag and name of the country.
 */
struct IP2CCountry
{
	/// All countries.
	static QHash<QString, IP2CCountry> all();

	const QPixmap *flag;
	QString name;

	IP2CCountry();
	explicit IP2CCountry(QString name);
	IP2CCountry(const QPixmap *flag, QString name);

	bool isValid() const;
};

#endif
